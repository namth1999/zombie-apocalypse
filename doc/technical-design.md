# Technisch ontwerp / Technical design

## Introduction

### 1. System Requirements:

   This game requires an android device running on API 23(minimum) and the main target
   API is 28(Oreo).
   
### 2. Overview
    
    The actors and items in the game is spawned by  `GameObjects` with all the graphics
    and movements attributes
    
    
    Game physics, HUD, controller and FPS of the game is controlled by the `GameEngine` class which
    includes the list of `InputObserver` interfaces to handle touch movement.
   
    `Animator` class: Decides the movement of all animated objects
  
  
    `BitmapStore`: Control the bitmap of the game
  
  
    `SoundEngine` class: Playing sound everytime user click on button.
  
  
    `Camera` class:Switch view of the game according to the position of Character
  
  
    `ZombieApocalypseActivity` class: Control onResume, onPause, initialize the game.
  
  
    `UIController` class: Handles user input
  
  
    `Renderer`: draw the game and also set the character in the center of the camera
  
  
    `PhysicsEngine` : detect collision
  
  
    `HUD`: Display game buttons to the screen
  
  
    `GameState`: Control the overall state of the game including coins remaining, maps, level,death, start, end game,...
  
  
    `GameObjectFactory`: generate objects of the game
  
  
    `LevelGrave`,`LevelMountains`,`LevelUnderground`: 3 maps of the game
  
  
    `LevelManager`: Handles map selection based on user touch
  
  
    `Level`: Used as a base for creating new maps
  
  
    GOSpec package: object model for maps of the game
     
### 3. Known Bugs   
    There is a bug in the android emulator at the game selection screen: It could take a while after user
    double click on the screen to go the game scene
    
    Sometimes player will automatically move through all the objects after spawned
    
    These bugs will not appear when running on a real device.
    
    For the best experience please run the game on an android device.

### 4. Class Diagram

```plantuml
@startuml
class ZombieApocalypseActivity{
+stopThread() : void
+startThread() : void
}
ZombieApocalypseActivity --> GameEngine
GameEngine --|> SurfaceView
class SurfaceView <<lib>> {
+onTouchEvent(MotionEvent motionEvent) : boolean
}
GameEngine --> GameState
GameEngine --> HUD
GameEngine --> UIController
UIController ..|> InputObserver
interface InputObserver{
+handleInput(MotionEvent event, GameState gameState, ArrayList<Rect> buttons) : void
}
GameEngine --> LevelManager
GameEngine --> PhysicsEngine
GameEngine --> Renderer
GameObject --> AnimatedGraphicComponent



class GameEngine{
-Thread thread
+GameEngine(Context c, Point size)
}

GameEngine ..|> Runnable
GameEngine ..|> GameEngineBroadcaster
GameEngine ..|> EngineController

interface Runnable{
+run() : void
}
interface GameEngineBroadcaster {
+addObserver(InputObserver o) : void
}

interface EngineController {
+startNewLevel() : void
}

class Renderer{
-Canvas canvas
-SurfaceHolder mSurfaceHolder
-Paint mPanit
+Renderer(SurfaceView sh, Point screenSize)
+getPixelsPerMetre() : int
+draw(ArrayList<GameObject> objects, GameState gs, HUD hud) : void
}

Renderer --> Camera

class PhysicsEngine{
+update(long fps, ArrayList<GameObject> objects, GameState gs) : void
+detectCollisions(GameState gs, ArrayList<GameObject> objects) : void
}

class LevelManager{
{static} +int PLAYER_INDEX = 0
    -ArrayList<GameObject> objects
    -GameObjectFactory factory
    +LevelManager(Context context, GameEngine ge, int pixelsPerMetre)
    +setCurrentLevel(String level) : void
    +getGameObjects() : ArrayList<GameObject>
    
}

LevelManager --> Level
abstract class Level {
-ArrayList<String> tiles
}
Level <|-- LevelGrave
Level <|-- LevelMountains
Level <|-- LevelUnderGround

class UIController{
-float mThird
-boolean initialPress : false
+UIController(GameEngineBroadcaster b, Point size)
+addObserver(GameEngineBroadcaster b) : void
}

HUD --> Bitmap
class Bitmap <<lib>>

class HUD{
-Bitmap menuBackgroundBitmap;
-Bitmap leftBitmap;
-rightBitmap;
-Bitmap jumpBitmap;
-Context c;
-int mTextFormatting;
-int mScreenHeight;
-int mScreenWidth;
-int buttonWidth;
-int buttonHeight;
-int buttonPadding;
-final float ONE_THIRD = .33f;
-final float TWO_THIRDS = .66f;
-ArrayList<Rect> mControls;
+{static}int LEFT = 0;
+{static}int RIGHT = 1;
+{static}int JUMP = 2;
+HUD(Context context, Point size)
+draw(Canvas c, Paint p, GameState gs) : void
+getControls() : Arraylist<Rect>
}

class GameState{
-{static} volatile boolean mThreadRunning : false
-{static} volatile boolean mPaused : true
-{static} volatile boolean mGameOver : true
-{static} volatile boolean mDrawing : false
-EngineController engineController
-int mFastestUnderground
-int mFastestMountains
-int mFastestGrave
-long startTimeInMillis
-int mCoinsAvailable
-int coinsCollected
-SharedPreferences.Editor editor
-String currentLevel
+GameState(EngineController gs, Context context)
+coinCollected() : void
+getCoinsRemaining() : void
+coinAddedToLevel() : void
+resetCoins() : void
+setCurrentLevel(String level) : void
+getCurrentLevel() : void
+objectiveReached() : void
+getFastestUnderground() : int
+getFastestMountains() : int
+getFastestCity() : int
+stopEverythings() : void
+startEverything() : void
+stopThread() : void
+getThreadRunning() : boolean
+startThread() : void
+getDrawing() : boolean
+getPaused() : boolean
+getGameOver() : boolean
+getCurrentTime() : int
+death() : void
+endGame() : void
}

class AnimatedGraphicComponent{
- String mBitmapName
- Animator mAnimator
- Rect mSectionToDraw
+ initialize(Context context,GameObjectSpec spec,PointF objectSize,int pixelsPerMetre) : void
+ draw(Canvas canvas, Paint paint, Transform t, Camera cam) : void
}
AnimatedGraphicComponent ..|> GraphicComponent
interface GraphicComponent {
+ initialize(Context c, GameObjectSpec spec, PointF objectSize, int pixelsPerMetre) : void
+ draw(Canvas canvas, Paint paint, Transform t, Camera cam) : void
}


class BackgroundGraphicComponent{
- String mBitmapName
+ initialize(Context context,GameObjectSpec spec,PointF objectSize,int pixelsPerMetre):void
+ draw(Canvas canvas, Paint paint, Transform t, Camera cam) : void
}
BackgroundGraphicComponent ..|> GraphicComponent


class BackgroundUpdateComponent{
+ update(long fps, Transform t, Transform playerTransform): void
}
BackgroundUpdateComponent..|>UpdateComponent
interface UpdateComponent {
+ update(long fps, Transform t, Transform playerTransform): void
} 


class DecorativeBlockUpdateComponent {
+ update(long fps, Transform t, Transform playerTransform): void
}
DecorativeBlockUpdateComponent..|> UpdateComponent


class EnemyUpdateComponent{
+ update(long fps, Transform t, Transform playerTransform): void
}
EnemyUpdateComponent..|>UpdateComponent


class InanimateBlockGraphicsComponent{
- String mBitmapName
+ initialize(Context c, GameObjectSpec spec, PointF objectSize, int pixelsPerMetre):void
+ draw(Canvas canvas, Paint paint, Transform t, Camera cam) : void
}
InanimateBlockGraphicsComponent..|> GraphicComponent


class InanimateBlockUpdateComponent {
- boolean mColliderNotSet = true
+ update(long fps,Transform t,Transform playerTransform) : void
}
InanimateBlockUpdateComponent..|>UpdateComponent


class MovableBlockUpdateComponent{
+ update(long fps, Transform t, Transform playerTransform): void
}
MovableBlockUpdateComponent..|>UpdateComponent


class PlayerInputComponent{
- Transform mPlayersTransform
- PlayerTransform mPlayersPlayerTransform
+ PlayerInputComponent(GameEngine ger)
+ setTransform(Transform transform): void
+ handleInput(MotionEvent event, GameState gs, ArrayList<Rect> buttons): void
}
PlayerInputComponent..>InputObserver


class PlayerUpdateComponent{
- boolean mIsJumping = false
- long mJumpStartTime
- final long MAX_JUMP_TIME = 400
- final float GRAVITY = 6
+ update(long fps, Transform t, Transform playerTransform): void
}
PlayerUpdateComponent..|>UpdateComponent

Renderer-->GameObject
PhysicsEngine-->GameObject

class GameObject{
- Transform mTransform
- boolean mActive = true
- String mTag
- GraphicsComponent mGraphicsComponent
- UpdateComponent mUpdateComponent
+ setGraphics(GraphicsComponent g,Context c,GameObjectSpec spec,PointF objectSize,int pixelsPerMetre): void
+ setMovement(UpdateComponent m): void
+ setPlayerInputTransform(PlayerInputComponent s): void
+ setTransform(Transform t): void
+ draw(Canvas canvas, Paint paint, Camera cam): void
+ update(long fps, Transform playerTransform): void
+ checkActive(): boolean
+ getTag(): String
+ setInactive(): void
+ getTransform(): Transform
+  setTag(String tag): void
}

GameEngine-->BitmapStore
class BitmapStore{
- Map<String, Bitmap> mBitmapsMap
- Map<String, Bitmap> mBitmapsReversedMap
- BitmapStore mOurInstance
+ getInstance(Context context): BitmapStore
+ BitmapStore(Context c)
+ getBitmap(String bitmapName): Bitmap
+ getBitmapReversed(String bitmapName): Bitmap
+ addBitmap(Context c,String bitmapName,PointF objectSize,int pixelsPerMetre,boolean needReversed): void
+ clearStore(): void
}

GameEngine--> SoundEngine
class SoundEngine{
- SoundPool mSP
- int mJump_ID = -1
- int mReach_Objective_ID = -1
- int mCoin_Pickup_ID = -1
- int mPlayer_Burn_ID = -1
- SoundEngine ourInstance
+ getInstance(Context context): SoundEngine
+ SoundEngine(Context c)
+ playJump(): void
+ playReachObjective(): void
+ playCoinPickup(): void
+ playPlayerBurn(): void
}

LevelManager-->GameObjectFactory
class GameObjectFactory{
- Context mContext
- GameEngine mGameEngineReference
- int mPixelsPerMetre
+ GameObjectFactory(Context context,GameEngine gameEngine,int pixelsPerMetre)
+ create(GameObjectSpec spec, PointF location): GameObject
}

AnimatedGraphicComponent-->Animator
class Animator{
- int mFrameCount
- int mCurrentFrame
- long mFrameTicker
- int mFramePeriod
- int mFrameWidth
+ Animator(float frameHeight,float frameWidth,int frameCount,int pixelsPerMetre)
+ getCurrentFrame(long time): Rect
}

abstract class GameObjectSpec{
- String mTag
- String mBitmapName
- float mSpeed
- PointF mSize
- String[] mComponents
- int mFramesAnimation
+ GameObjectSpec(String tag,String bitmapName,float speed,PointF size,String[] components,int framesAnimation )
+ getNumFrames(): int
+ getTag(): String
+ getBitmapName(): String
+ getSpeed(): float
+ getSize(): PointF
+ getComponents(): String[]
}

LevelManager-->BackgroundGraveyardSpec
class BackgroundGraveyardSpec{
- final String tag = "Background"
- final String bitmapName = "background_zombie"
- final int framesOfAnimation = 1
- final float speed = 3f
- final PointF size = new PointF(100, 70)
- final String[] components = new String[] { "BackgroundGraphicsComponent","BackgroundUpdateComponent"}
+ BackgroundGraveyardSpec()
}
GameObjectSpec ..|> BackgroundGraveyardSpec

LevelManager-->BackgroundMountainSpec
class BackgroundMountainSpec{
- final String tag = "Background"
- final String bitmapName = "mountain_background"
- final int framesOfAnimation = 1
- final float speed = 3f
- final PointF size = new PointF(100, 70)
- final String[] components = new String[] {"BackgroundGraphicsComponent","BackgroundUpdateComponent"}
+ BackgroundMountainSpec()
}
GameObjectSpec ..|> BackgroundMountainSpec

LevelManager-->BackgroundUndergroundSpec
class BackgroundUndergroundSpec{
- final String tag = "Background"
- final String bitmapName = "underground_background"
- final int framesOfAnimation = 1
- final float speed = 3f
- final PointF size = new PointF(100, 70f)
- final String[] components = new String[] {"BackgroundGraphicsComponent","BackgroundUpdateComponent"}
+ BackgroundUndergroundSpec()
}
GameObjectSpec ..|>BackgroundUndergroundSpec

BackgroundGraveyardSpec-->BackgroundGraphicComponent
BackgroundMountainSpec-->BackgroundGraphicComponent
BackgroundUndergroundSpec-->BackgroundGraphicComponent

BackgroundGraveyardSpec-->BackgroundUpdateComponent
BackgroundMountainSpec-->BackgroundUpdateComponent
BackgroundUndergroundSpec-->BackgroundUpdateComponent


LevelManager-->BrickTileSpec
class BrickTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "brick"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[]{"InanimateBlockGraphicsComponent","InanimateBlockUpdateComponent"}
+ BrickTileSpec()
}
GameObjectSpec ..|>BrickTileSpec
BrickTileSpec-->InanimateBlockGraphicsComponent
BrickTileSpec-->InanimateBlockUpdateComponent


LevelManager-->CartTileSpec
class CartTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "cart"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(2f, 1f)
- final String[] components = new String[]{"InanimateBlockGraphicsComponent","InanimateBlockUpdateComponent"}
+ CartTileSpec()
}
GameObjectSpec ..|>CartTileSpec
CartTileSpec-->InanimateBlockGraphicsComponent
CartTileSpec-->InanimateBlockUpdateComponent


LevelManager-->CoalTileSpec
class CoalTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "coal"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent","InanimateBlockUpdateComponent"}
+ CoalTileSpec()
}
GameObjectSpec ..|>CoalTileSpec
CoalTileSpec-->InanimateBlockGraphicsComponent
CoalTileSpec-->InanimateBlockUpdateComponent


LevelManager-->CollectibleObjectSpec
class CollectibleObjectSpec{
- final String tag = "Collectible"
- final String bitmapName = "coin"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent","InanimateBlockUpdateComponent"}
+ CollectibleObjectSpec()
}
GameObjectSpec ..|>CollectibleObjectSpec
CollectibleObjectSpec-->InanimateBlockGraphicsComponent
CollectibleObjectSpec-->InanimateBlockUpdateComponent


LevelManager-->ConcreteTileSpec
class ConcreteTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "concrete"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent","InanimateBlockUpdateComponent"}
+ ConcreteTileSpec()
}
GameObjectSpec ..|>ConcreteTileSpec
ConcreteTileSpec-->InanimateBlockGraphicsComponent
ConcreteTileSpec-->InanimateBlockUpdateComponent


LevelManager-->DeadTreeTileSpec
class DeadTreeTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "dead_tree"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(2f, 4f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent","DecorativeBlockUpdateComponent"}
+ DeadTreeTileSpec()
}
GameObjectSpec ..|>DeadTreeTileSpec
DeadTreeTileSpec-->InanimateBlockGraphicsComponent
DeadTreeTileSpec-->DecorativeBlockUpdateComponent


LevelManager-->EnemySpec
class EnemySpec{
- final String tag = "Enemy"
- final String bitmapName = "villain"
- final int framesOfAnimation = 10
- final float speed = 10f
- final PointF size = new PointF(2f, 2f)
- final String[] components = new String[] {"AnimatedGraphicsComponent","EnemyUpdateComponent"}
+ EnemySpec()
}
GameObjectSpec ..|>EnemySpec
EnemySpec-->AnimatedGraphicsComponent
EnemySpec-->EnemyUpdateComponent


LevelManager-->FireTileSpec
class FireTileSpec{
- final String tag = "Death"
- final String bitmapName = "fire"
- final int framesOfAnimation = 3
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[]{"AnimatedGraphicsComponent","InanimateBlockUpdateComponent"}
+ FireTileSpec()
}
GameObjectSpec ..|>FireTileSpec
FireTileSpec-->AnimatedGraphicsComponent
FireTileSpec-->InanimateBlockUpdateComponent


LevelManager-->GraveGroundTileSpec
class GraveGroundTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "turf"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[]{"InanimateBlockGraphicsComponent", "InanimateBlockUpdateComponent"}
+ GraveGroundTileSpec()
}
GameObjectSpec ..|>GraveGroundTileSpec
GraveGroundTileSpec-->InanimateBlockGraphicsComponent
GraveGroundTileSpec-->InanimateBlockUpdateComponent


LevelManager-->InvisibleDeathTenByTenSpec
class InvisibleDeathTenByTenSpec{
- final String tag = "Death"
- final String bitmapName = "invisible_death"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(10f, 10f)
- final String[] components = new String[]{"InanimateBlockGraphicsComponent", "InanimateBlockUpdateComponent"}
+ InvisibleDeathTenByTenSpec()
}
GameObjectSpec ..|>InvisibleDeathTenByTenSpec
InvisibleDeathTenByTenSpec-->InanimateBlockGraphicsComponent
InvisibleDeathTenByTenSpec-->InanimateBlockUpdateComponent


LevelManager-->LamppostTileSpec
class LamppostTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "lamppost"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 4f)
- final String[] components = new String[]{"InanimateBlockGraphicsComponent", "DecorativeBlockUpdateComponent"}
+ LamppostTileSpec()
}
GameObjectSpec ..|>LamppostTileSpec
LamppostTileSpec-->InanimateBlockGraphicsComponent
LamppostTileSpec-->DecorativeBlockUpdateComponent


LevelManager-->MoveablePlatformSpec
class MoveablePlatformSpec{
- final String tag = "Movable Platform"
- final String bitmapName = "platform"
- final int framesOfAnimation = 1
- final float speed = 3f
- final PointF size = new PointF(2f, 1f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent","MovableBlockUpdateComponent"}
+ MoveablePlatformSpec()
}
GameObjectSpec ..|>MoveablePlatformSpec
MoveablePlatformSpec-->InanimateBlockGraphicsComponent
MoveablePlatformSpec-->MovableBlockUpdateComponent


LevelManager-->ObjectiveTileSpec
class ObjectiveTileSpec{
- final String tag = "Objective Tile"
- final String bitmapName = "objective"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(3f, 3f)
- final String[] components = new String[]{"InanimateBlockGraphicsComponent", "InanimateBlockUpdateComponent"}
+ ObjectiveTileSpec()
}
GameObjectSpec ..|>ObjectiveTileSpec
ObjectiveTileSpec-->InanimateBlockGraphicsComponent
ObjectiveTileSpec-->InanimateBlockUpdateComponent


LevelManager-->PlayerSpec
class PlayerSpec{
- final String tag = "Player"
- final String bitmapName = "player"
- final int framesOfAnimation = 5
- final float speed = 10f
- final PointF size = new PointF(1f, 2f)
- final String[] components = new String[] {
              "PlayerInputComponent",
              "AnimatedGraphicsComponent",
              "PlayerUpdateComponent"}
+ PlayerSpec()
}
GameObjectSpec ..|>PlayerSpec
PlayerSpec-->PlayerInputComponent
PlayerSpec-->AnimatedGraphicsComponent
PlayerSpec-->PlayerUpdateComponent



LevelManager-->ScorchedTileSpec
class ScorchedTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "scorched"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent", "InanimateBlockUpdateComponent"}
+ ScorchedTileSpec()
}
GameObjectSpec ..|>ScorchedTileSpec
ScorchedTileSpec-->InanimateBlockGraphicsComponent
ScorchedTileSpec-->InanimateBlockUpdateComponent


LevelManager-->SnowTileSpec
class SnowTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "snow"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(1f, 1f)
- final String[] components = new String[] { "InanimateBlockGraphicsComponent",  "InanimateBlockUpdateComponent"}
+ SnowTileSpec()
}
GameObjectSpec ..|>SnowTileSpec
SnowTileSpec-->InanimateBlockGraphicsComponent
SnowTileSpec-->InanimateBlockUpdateComponent


LevelManager-->SnowyTreeTileSpec
class SnowyTreeTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "snowy_tree"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(3f, 6f)
- final String[] components = new String[] { "InanimateBlockGraphicsComponent","DecorativeBlockUpdateComponent"}
+ SnowyTreeTileSpec()
}
GameObjectSpec ..|>SnowyTreeTileSpec
SnowyTreeTileSpec-->InanimateBlockGraphicsComponent
SnowyTreeTileSpec-->DecorativeBlockUpdateComponent


LevelManager-->StalactiteTileSpec
class StalactiteTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "stalactite"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(2f, 4f)
- final String[] components = new String[] {  "InanimateBlockGraphicsComponent",  "DecorativeBlockUpdateComponent"}
+ StalactiteTileSpec()
}
GameObjectSpec ..|>StalactiteTileSpec
StalactiteTileSpec-->InanimateBlockGraphicsComponent
StalactiteTileSpec-->DecorativeBlockUpdateComponent


LevelManager-->StalagmiteTileSpec
class StalagmiteTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "stalagmite"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(2f, 4f)
- final String[] components = new String[] {"InanimateBlockGraphicsComponent","DecorativeBlockUpdateComponent"}
+ StalagmiteTileSpec()
}
GameObjectSpec ..|>StalagmiteTileSpec
StalagmiteTileSpec-->InanimateBlockGraphicsComponent
StalagmiteTileSpec-->DecorativeBlockUpdateComponent


LevelManager-->StonePileTileSpec
class StonePileTileSpec{
- final String tag = "Inert Tile"
- final String bitmapName = "stone_pile"
- final int framesOfAnimation = 1
- final float speed = 0f
- final PointF size = new PointF(2f, 1f)
- final String[] components = new String[] { "InanimateBlockGraphicsComponent", "InanimateBlockUpdateComponent"}
+ StonePileTileSpec()
}
GameObjectSpec ..|>StonePileTileSpec
StonePileTileSpec-->InanimateBlockGraphicsComponent
StonePileTileSpec-->InanimateBlockUpdateComponent


LevelManager-->LaserSpec
class LaserSpec{
- final String tag = "laser"
- final String bitmapName = "player_laser"
- final int framesOfAnimation = 1
- final float speed = 15f
- final PointF size = new PointF(2f, 1f)
- final String[] components = new String[] { "InanimateBlockGraphicsComponent", "LaserUpdateComponent", 
"LaserSpawnComponent" }
+ LaserSpec()
}
GameObjectSpec ..|>LaserSpec
LaserSpec-->InanimateBlockGraphicsComponent
LaserSpec-->LaserUpdateComponent
LaserSpec-->LaserSpawnComponent

















@enduml

