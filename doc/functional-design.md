## Functional design
`Zombie Apocalypse` is a platform game about a hero trying to save the world. In 2020, a millionaire who obsessed about immortal life. He builds his own secret labs and testing real on human. Eventually, the lab successfully created a virus which keeps test subject number 7 stay undead. But what goes around comes around, the test subject mental is greatly damaged, he quickly turns into a beast that hungry for fleshes and bloods. After the labs is not strong enough to hold him back, he infects the whole town and the apocalypse started. Now our hero who daughter is infected must find the first parent, take his blood in order to create a cure.

### Menu screen
This is the main activity, that is opened when tapping the app icon.

![Menu wireframe](menu.jpg)
- A. Name of the missions, 3 in total.
- B. Fastest time to complete each mission (lower is better).
- C. Game's title.
- D. still image of each mission, player can double tap on those image to go to each mission respectively, screen will go to either one of these [mission 1](#gameplay-screen-mission-1) [mission 2](#gameplay-screen-mission-2) [mission 3](#gameplay-screen-mission-3) based on player selection.
- E. Instruction for player.

### Gameplay screen mission 1
![Menu wireframe](gameplay1.jpg)
- F. Timer counting up, it will start at 500 unit, go up 1 unit each second, for each money `G` hero picked up, timer will go down 10 unit.
- G. Money, will disappear when hero go through and decrease timer `F` 10 unit.
- H. Fixed tiles/platform, the hero cannot move through this object and can only stand on it.
- I. The hero, player can move the hero left or right using `O` and `P` button, jump using `Q` button.
- J. Moving platform, going vertically, hero can stand on it.
- K. Decoration for each mission, hero can move through this object without harm.
- L. Fire, fixed position, obstacle for mission 1, if hero touch it then game is over.
- O. Left button, make hero go left.
- P. right button, make hero go right.
- Q. Jump button, make hero jump. Due to in-game gravity, up-movement is relatively fast, while down-movement is slower than up-movement.
- R. Gun button, hero will shoot out laser to the front.

### Gameplay screen mission 2
![Menu wireframe](gameplay2.jpg)
- F. Timer counting up, it will start at 500 unit, go up 1 unit each second, for each money `G` hero picked up, timer will go down 10 unit.
- G. Money, will disappear when hero go through and decrease timer `F` 10 unit.
- H. Fixed tiles/platform, the hero cannot move through this object and can only stand on it.
- I. The hero, player can move the hero left or right using `O` and `P` button, jump using `Q` button.
- J. Moving platform, going vertically, hero can stand on it.
- M. Ultimate goal of each mission, mission will be complete when hero touch it. Screen will go to [menu screen](#menu-screen) and `B` will be updated if the time is lower than 1000.
- O. Left button, make hero go left.
- P. right button, make hero go right.
- Q. Jump button, make hero jump. Due to in-game gravity, up-movement is relatively fast, while down-movement is slower than up-movement.
- R. Gun button, hero will shoot out laser to the front.

### Gameplay screen mission 3
![Menu wireframe](gameplay3.jpg)
- F. Timer counting up, it will start at 500 unit, go up 1 unit each second, for each money `G` hero picked up, timer will go down 10 unit.
- G. Money, will disappear when hero go through and decrease timer `F` 10 unit.
- H. Fixed tiles/platform, the hero cannot move through this object and can only stand on it.
- I. The hero, player can move the hero left or right using `O` and `P` button, jump using `Q` button.
- J. Moving platform, going vertically, hero can stand on it.
- K. Decoration for each mission, hero can move through this object without harm.
- N. Zombie, running very fast, patrolling on fixed line. game is over if hero touch it.
- O. Left button, make hero go left.
- P. right button, make hero go right.
- Q. Jump button, make hero jump. Due to in-game gravity, up-movement is relatively fast, while down-movement is slower than up-movement.
- R. Gun button, hero will shoot out laser to the front.

![Menu wireframe](laser.jpg)
- S. laser shooting out from hero, if zombie get hit by laser, zombie will die.
---
- Platforms, obstacles, zombies and money's position for all 3 missions will be hard coded, not auto generated.