# Code of conduct 

Team OP-6

## Team member
|Student number		|Full name		|E-mail address			 |
|:-----------------:|---------------|----------------------- |
|464560				|Nam Trinh		|464560@student.saxion.nl|
|467424				|Linh Hoang		|467424@student.saxion.nl|
|463701				|Huy Tran		|463701@student.saxion.nl|
|463255				|Xu Kang		|463255@student.saxion.nl|
|464392             |Huy Cao        |464392@student.saxion.nl|
## Timesheet

Each group member of the team will keep track of their individual working hours, specifying which activities they spent their time on.

## Working hours

Minimum hours per week: 15 (Including lecture, seminar and individual assignments)
There is one session per week of 4 hours every Thursday from 12:00 till 16:00.

## Working method

All tasks/issues that are need to be done will be created on GitLab. Tasks can be added when needed.

## Working tools

- **Google Drive** so all of us can collaborate simultaneously on this document (in markdown format).
- **Whatsapp** to discuss about what to do after the meeting, lecture.
- **Android Studio** to develop our game.
- **GitLab** to store our code so everyone can work on it simultaneously.

## Minor rules

These rules do not warrant a strike. Any subsequent violations after 3 times will also warrant a strike.

-   The team agrees upon the set limit of 20 minutes late, anything within 20 minutes will be accepted as long as the team member has a legitimate reason. After 20 minutes if the team has not been notified the team member is seen as too late.
-   If anything should change within this document the rest of the team has to be notified.
    
## Major rules

When breaking one of these rules the team member will get a strike.

-   If absent for project hours, the rest of the team should be notified in a timely manner
-   A task is not marked as done until the task is completely finished and accepted by a peer.
    
## Strikes

Breaking minor and major rules are all documented. Once a team member has received 3 strikes it is requested to the teacher that they are removed from the team or are penalized in their grade.

|Name       |Minor rule	|Major rule	|Note		|
|-----------|-----------|-----------|-----------|
|Nam Trinh	|			|			|			|
|Linh Hoang |			|			|			|
|Huy Tran   |			|			|			|
|Xu Kang    |			|			|			|
|Huy Cao    |          |           |           |


