/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.GOSpec;

import android.graphics.PointF;

/**
 * ANIMATED, MOVING OBJECT
 * @compo EnemyUpdateComponent handle moving, jumping, falling
 */

public class EnemySpec extends GameObjectSpec {
    // This is all the unique specifications for a player
    private static final String tag = "Enemy";
    private static final String bitmapName = "villain";

    //10 villain pic
    private static final int framesOfAnimation = 10;

    //speed in virtual metres per second
    private static final float speed = 10f;
    private static final PointF size = new PointF(2f, 2f);
    private static final String[] components = new String[] {
            "AnimatedGraphicsComponent",
            "EnemyUpdateComponent"};

    public EnemySpec() {
        super(tag, bitmapName, speed,
                size, components, framesOfAnimation);
    }
}
