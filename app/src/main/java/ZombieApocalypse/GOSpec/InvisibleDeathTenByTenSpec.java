package ZombieApocalypse.GOSpec;

import android.graphics.PointF;

/**
 * COLLIDE = DEATH OBJECT
 */

public class InvisibleDeathTenByTenSpec extends GameObjectSpec {
    private static final String tag = "Death";
    private static final String bitmapName = "invisible_death";
    private static final int framesOfAnimation = 1;
    private static final float speed = 0f;
    private static final PointF size = new PointF(10f, 10f);
    private static final String[] components = new String[]{"InanimateBlockGraphicsComponent", "InanimateBlockUpdateComponent"};

    public InvisibleDeathTenByTenSpec() {
        super(tag, bitmapName, speed, size, components, framesOfAnimation);
    }
}
