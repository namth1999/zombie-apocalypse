/*
 *
 *  * *
 *  *  * Created by Huy Tran
 *  *  * Copyright (c) 2019 . All rights reserved.
 *  *
 *
 */

package ZombieApocalypse.GOSpec;

import android.graphics.PointF;

/**
 * BACKGROUND OF CITY LEVEL
 */

public class BackgroundGraveyardSpec extends GameObjectSpec {
    // This is all the unique specifications for the city background
    private static final String tag = "Background";
    private static final String bitmapName = "background_zombie";
    private static final int framesOfAnimation = 1;
    private static final float speed = 3f;
    private static final PointF size = new PointF(100, 70);
    private static final String[] components = new String[] {
            "BackgroundGraphicsComponent","BackgroundUpdateComponent"};

    public BackgroundGraveyardSpec() {
        super(tag, bitmapName, speed, size, components, framesOfAnimation);
    }
}
