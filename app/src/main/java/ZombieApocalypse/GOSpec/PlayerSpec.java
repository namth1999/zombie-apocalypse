/*
 * *
 *  * Created by Linh Hoang
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */

package ZombieApocalypse.GOSpec;

import android.graphics.PointF;

/**
 * ANIMATED, INPUT-RECEIVING OBJECT
 * @compo PlayerUpdateComponent handle moving, jumping, falling
 * @compo PlayerInputComponent handle screen touches
 */

public class PlayerSpec extends GameObjectSpec {
    // This is all the unique specifications for a player
    private static final String tag = "Player";
    private static final String bitmapName = "player";

    //có 5 hình player
    private static final int framesOfAnimation = 5;

    //speed in virtual metres per second
    private static final float speed = 10f;
    private static final PointF size = new PointF(1f, 2f);
    private static final String[] components = new String[] {
            "PlayerInputComponent",
            "AnimatedGraphicsComponent",
            "PlayerUpdateComponent"};

    public PlayerSpec() {
        super(tag, bitmapName, speed,
                size, components, framesOfAnimation);
    }
}
