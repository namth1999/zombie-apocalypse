/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.GOSpec;

import android.graphics.PointF;

public class LaserSpec extends GameObjectSpec {
    private static final String tag = "laser";
    private static final String bitmapName = "player_laser";
    private static final int framesOfAnimation = 1;
    private static final float speed = 15f;
    private static final PointF size = new PointF(2f, 1f);
    private static final String[] components = new String[] {
            "InanimateBlockGraphicsComponent",
            "LaserUpdateComponent",
            "LaserSpawnComponent"
            };

    public LaserSpec() {
        super(tag, bitmapName, speed, size,
                components, framesOfAnimation);
    }
}
