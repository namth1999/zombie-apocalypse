/*
 *
 *  * *
 *  *  * Created by Huy Tran
 *  *  * Copyright (c) 2019 . All rights reserved.
 *  *
 *
 */

package ZombieApocalypse.GOSpec;

import android.graphics.PointF;

/**
 * BACKGROUND OF MOUNTAIN LEVEL
 */

public class BackgroundMountainSpec extends GameObjectSpec {
    // This is all the specifications for the mountain
    private static final String tag = "Background";
    private static final String bitmapName = "mountain_background";
    private static final int framesOfAnimation = 1;
    private static final float speed = 3f;
    private static final PointF size = new PointF(100, 70);
    private static final String[] components = new String[] {
            "BackgroundGraphicsComponent",
            "BackgroundUpdateComponent"};

    public BackgroundMountainSpec() {
        super(tag, bitmapName, speed, size,
                components, framesOfAnimation);
    }
}
