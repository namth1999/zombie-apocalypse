/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import ZombieApocalypse.Component.LaserSpawnComponent;
import ZombieApocalypse.Component.PlayerInputComponent;
import ZombieApocalypse.GOSpec.GameObjectSpec;
import ZombieApocalypse.Interfaces.GraphicsComponent;
import ZombieApocalypse.Interfaces.SpawnComponent;
import ZombieApocalypse.Interfaces.UpdateComponent;
import ZombieApocalypse.Transform.PlayerTransform;
import ZombieApocalypse.Transform.Transform;

public class GameObject {
    private Transform mTransform;


    private boolean mActive = true;
    private String mTag;

    private GraphicsComponent mGraphicsComponent;
    private UpdateComponent mUpdateComponent;
    public SpawnComponent spawnComponent;

    public void setGraphics(GraphicsComponent g,
                            Context c,
                            GameObjectSpec spec,
                            PointF objectSize,
                            int pixelsPerMetre) {

        mGraphicsComponent = g;
        g.initialize(c, spec, objectSize, pixelsPerMetre);
    }

    public void setMovement(UpdateComponent m) {
        mUpdateComponent = m;
    }

    public void setPlayerInputTransform(PlayerInputComponent s) {
        s.setTransform(mTransform);
    }

    public void setTransform(Transform t) {
        mTransform = t;
    }

    public void draw(Canvas canvas, Paint paint, Camera cam) {
        mGraphicsComponent.draw(canvas,
                paint,
                mTransform, cam);
    }

    public void update(long fps, Transform playerTransform) {
        mUpdateComponent.update(fps,
                mTransform,
                playerTransform);
    }

    public boolean checkActive() {
        return mActive;
    }

    public String getTag() {
        return mTag;
    }

    public void setInactive() {
        mActive = false;
    }

    public Transform getTransform() {
        return mTransform;
    }

    public void setTag(String tag) {
        mTag = tag;
    }

    public  void  setActive(){
        mActive = true;
    }

//    public boolean spawn(PlayerTransform playerTransform) {
//        // Only spawnComponent if not already active
//        if (!mActive) {
//            spawnComponent.spawn(playerTransform, mTransform);
//            mActive = true;
//            return true;
//        }
//        return false;
//    }

    public void setSpawner(SpawnComponent s) {
        spawnComponent = s;
    }

    public boolean spawn(PlayerTransform playerTransform) {
        // Only spawnComponent if not already active
        if (!checkActive()) {
            spawnComponent.spawn(playerTransform, mTransform);
            setActive();
            return true;
        }
        return false;
    }
}
