/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

import ZombieApocalypse.Levels.LevelManager;

public class Renderer {
    private Canvas mCanvas;
    private SurfaceHolder mSurfaceHolder;
    private Paint mPaint;

    // Here is our new camera
    private Camera mCamera;

    public Renderer(SurfaceView sh, Point screenSize){
        mSurfaceHolder = sh.getHolder();
        mPaint = new Paint();

        // Initialize the camera
        mCamera = new Camera(screenSize.x, screenSize.y);
    }

    public int getPixelsPerMetre(){
        return mCamera.getPixelsPerMetre();
    }

    /**
     * Draw game
     * @param objects
     * @param gs
     * @param hud
     */
    public void draw(ArrayList<GameObject> objects,
              GameState gs,
              HUD hud) {

        if (mSurfaceHolder.getSurface().isValid()) {
            mCanvas = mSurfaceHolder.lockCanvas();
            mCanvas.drawColor(Color.argb(255, 0, 0, 0));

            if(gs.getDrawing()) {
                // Set the player as the center of the camera
                mCamera.setWorldCentre(
                        objects.get(LevelManager
                                .PLAYER_INDEX)
                                .getTransform().getLocation());

                for (int i=0;i<objects.size();i++) {
                    if (objects.get(i).checkActive()) {
                        objects.get(i).draw(mCanvas, mPaint,
                                mCamera);
                    }
                }
            }

            hud.draw(mCanvas, mPaint, gs);

            mSurfaceHolder.unlockCanvasAndPost(mCanvas);
        }
    }
}
