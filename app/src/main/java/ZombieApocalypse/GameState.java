/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse;

import android.content.Context;
import android.content.SharedPreferences;

import ZombieApocalypse.Interfaces.EngineController;
import ZombieApocalypse.Levels.LevelManager;

public final class GameState {
    private static volatile boolean
            mThreadRunning = false;

    private static volatile boolean mPaused = true;
    private static volatile boolean mGameOver = true;
    private static volatile boolean mDrawing = false;
    private EngineController engineController;

    private int mFastestUnderground;
    private int mFastestMountains;
    private int mFastestGrave;
    private long startTimeInMillis;

    private int mCoinsAvailable;
    private int coinsCollected;

    private SharedPreferences.Editor editor;

    private String currentLevel;

    public GameState(EngineController gs, Context context) {
        engineController = gs;
        SharedPreferences prefs = context
                .getSharedPreferences("HiScore",
                        Context.MODE_PRIVATE);

        editor = prefs.edit();
        mFastestUnderground = prefs.getInt(
                "fastest_underground_time", 1000);
        mFastestMountains = prefs.getInt(
                "fastest_mountains_time", 1000);
        mFastestGrave = prefs.getInt(
                "fastest_grave_time", 1000);
    }

    public void coinCollected() {
        coinsCollected++;
    }

    public int getCoinsRemaining() {
        return mCoinsAvailable - coinsCollected;
    }

    public void coinAddedToLevel() {
        mCoinsAvailable++;
    }

    public void resetCoins() {
        mCoinsAvailable = 0;
        coinsCollected = 0;
    }

    public void setCurrentLevel(String level) {
        currentLevel = level;
    }

    public String getCurrentLevel() {
        return currentLevel;
    }

    public void objectiveReached() {
        endGame();
    }

    public int getFastestUnderground() {
        return mFastestUnderground;
    }

    public int getFastestMountains() {
        return mFastestMountains;
    }

    public int getFastestCity() {
        return mFastestGrave;
    }

    public void stopEverything() {// Except the thread
        mPaused = true;
        mGameOver = true;
        mDrawing = false;
    }

    private void startEverything() {
        mPaused = false;
        mGameOver = false;
        mDrawing = true;
    }

    public void stopThread() {
        mThreadRunning = false;
    }

    public boolean getThreadRunning() {
        return mThreadRunning;
    }

    public void startThread() {
        mThreadRunning = true;
    }

    public boolean getDrawing() {
        return mDrawing;
    }

    public boolean getPaused() {
        return mPaused;
    }

    public boolean getGameOver() {
        return mGameOver;
    }

    public void startNewGame() {
        // Don't want to be handling objects while
        // clearing ArrayList and filling it up again
        stopEverything();
        engineController.startNewLevel();
        startEverything();
        startTimeInMillis = System.currentTimeMillis();
    }

    public int getCurrentTime() {
        long MILLIS_IN_SECOND = 1000;
        return (int) ((System.currentTimeMillis()
                - startTimeInMillis) / MILLIS_IN_SECOND);
    }

    /**
     * end game when player is dead
     */
    public void death() {
        LevelManager.createdLaser = false;
        stopEverything();
        SoundEngine.playPlayerBurn();
    }

    private void endGame() {

        stopEverything();
        int totalTime =
                ((mCoinsAvailable - coinsCollected)
                        * 10)
                        + getCurrentTime();

        switch (currentLevel) {

            case "underground":
                if (totalTime < mFastestUnderground) {
                    mFastestUnderground = totalTime;
                    // Save new time

                    editor.putInt("fastest_underground_time",
                            mFastestUnderground);

                    editor.commit();
                }
                break;
            case "grave":
                if (totalTime < mFastestGrave) {
                    mFastestGrave = totalTime;
                    // Save new time
                    editor.putInt("fastest_grave_time",
                            mFastestGrave);

                    editor.commit();
                }
                break;
            case "mountains":
                if (totalTime < mFastestMountains) {
                    mFastestMountains = totalTime;
                    // Save new time
                    editor.putInt("fastest_mountains_time",
                            mFastestMountains);

                    editor.commit();
                }
                break;
        }
    }
}