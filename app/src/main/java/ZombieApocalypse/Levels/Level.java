/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Levels;

import java.util.ArrayList;

public abstract class Level {
    // If you want to build a new level then extend this class
    public ArrayList<String> tiles;
    public ArrayList<String> getTiles(){
        return tiles;
    }
}