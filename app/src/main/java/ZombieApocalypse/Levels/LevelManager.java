/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Levels;

import android.content.Context;
import android.graphics.PointF;
import android.util.Log;

import java.util.ArrayList;

import ZombieApocalypse.GOSpec.BackgroundGraveyardSpec;
import ZombieApocalypse.GOSpec.BackgroundMountainSpec;
import ZombieApocalypse.GOSpec.BackgroundUndergroundSpec;
import ZombieApocalypse.GOSpec.CartTileSpec;
import ZombieApocalypse.GOSpec.CoalTileSpec;
import ZombieApocalypse.GOSpec.CollectibleObjectSpec;
import ZombieApocalypse.GOSpec.ConcreteTileSpec;
import ZombieApocalypse.GOSpec.DeadTreeTileSpec;
import ZombieApocalypse.GOSpec.EnemySpec;
import ZombieApocalypse.GOSpec.FireTileSpec;
import ZombieApocalypse.GOSpec.GraveGroundTileSpec;
import ZombieApocalypse.GOSpec.BrickTileSpec;
import ZombieApocalypse.GOSpec.InvisibleDeathTenByTenSpec;
import ZombieApocalypse.GOSpec.LamppostTileSpec;
import ZombieApocalypse.GOSpec.LaserSpec;
import ZombieApocalypse.GOSpec.MoveablePlatformSpec;
import ZombieApocalypse.GOSpec.ObjectiveTileSpec;
import ZombieApocalypse.GOSpec.PlayerSpec;
import ZombieApocalypse.GOSpec.ScorchedTileSpec;
import ZombieApocalypse.GOSpec.SnowTileSpec;
import ZombieApocalypse.GOSpec.SnowyTreeTileSpec;
import ZombieApocalypse.GOSpec.StalactiteTileSpec;
import ZombieApocalypse.GOSpec.StalagmiteTileSpec;
import ZombieApocalypse.GOSpec.StonePileTileSpec;
import ZombieApocalypse.GameEngine;
import ZombieApocalypse.GameObject;
import ZombieApocalypse.GameObjectFactory;
import ZombieApocalypse.GameState;
import ZombieApocalypse.Transform.PlayerTransform;

public class LevelManager {
    public static int mNextPlayerLaser;
    public static boolean createdLaser = false;
    public static int FIRST_PLAYER_LASER;
    public static int LAST_PLAYER_LASER;
    public static int PLAYER_INDEX = 0;
    private ArrayList<GameObject> objects;
    ArrayList<String> levelToLoad;
    private Level currentLevel;
    private GameObjectFactory factory;

    public LevelManager(Context context,
                        GameEngine ge,
                        int pixelsPerMetre) {

        objects = new ArrayList<>();
        factory = new GameObjectFactory(context,
                ge,
                pixelsPerMetre);
    }

    /**
     * Set the map base on user touch
     * @param level
     */
    public void setCurrentLevel(String level) {
        switch (level) {
            case "underground":
                currentLevel = new LevelUnderground();
                break;

            case "grave":
                currentLevel = new LevelGrave();
                break;

            case "mountains":
                currentLevel = new LevelMountains();
                break;
        }
    }

    public ArrayList<GameObject> getGameObjects() {
        return objects;
    }

    /**
     * Backgrounds 1, 2, 3(City, Underground, Mountain...)
     * p = Player
     * g = Grass tile
     * o = Objective
     * m = Movable platform
     * b = Brick tile
     * c = mine Cart
     * s = Stone pile
     * l = coaL
     * n = coNcrete
     * a = lAmpost
     * r = scoRched tile
     * w = snoW tile
     * t = stalacTite
     * i = stalagmIte
     * d = Dead tree
     * e = snowy trEe
     * x = Collectable
     * z = Fire
     * v = Villian
     * y = invisible death_invisible
     * - = laser
     */
    public void buildGameObjects(GameState gs) {
        gs.resetCoins();
        objects.clear();
        levelToLoad = currentLevel.getTiles();

        for (int row = 0; row < levelToLoad.size(); row++) {
            for (int column = 0;
                 column < levelToLoad.get(row).length();
                 column++) {

                PointF coords = new PointF(column, row);

                switch (levelToLoad.get(row)
                        .charAt(column)) {

                    case '1':
                        objects.add(factory.create(
                                new BackgroundGraveyardSpec(),
                                coords));
                        break;

                    case '2':
                        objects.add(factory.create(
                                new BackgroundUndergroundSpec(),
                                coords));
                        break;

                    case '3':
                        objects.add(factory.create(
                                new BackgroundMountainSpec(),
                                coords));
                        break;

                    case 'p':
                        objects.add(factory.create(new
                                        PlayerSpec(),
                                coords));
                        // Remember the location of
                        // the player
                        PLAYER_INDEX = objects.size() - 1;
                        break;

                    case 'g':
                        objects.add(factory.create(
                                new GraveGroundTileSpec(),
                                coords));
                        break;

                    case 'o':
                        objects.add(factory.create(
                                new ObjectiveTileSpec(),
                                coords));
                        break;

                    case 'm':
                        objects.add(factory.create(
                                new MoveablePlatformSpec(),
                                coords));
                        break;

                    case 'b':
                        objects.add(factory.create(
                                new BrickTileSpec(),
                                coords));
                        break;

                    case 'c':
                        objects.add(factory.create(
                                new CartTileSpec(),
                                coords));
                        break;

                    case 's':
                        objects.add(factory.create(
                                new StonePileTileSpec(),
                                coords));
                        break;

                    case 'l':
                        objects.add(factory.create(
                                new CoalTileSpec(),
                                coords));
                        break;

                    case 'n':
                        objects.add(factory.create(
                                new ConcreteTileSpec(),
                                coords));
                        break;

                    case 'a':
                        objects.add(factory.create(
                                new LamppostTileSpec(),
                                coords));
                        break;

                    case 'r':
                        objects.add(factory.create(
                                new ScorchedTileSpec(),
                                coords));
                        break;

                    case 'w':
                        objects.add(factory.create(
                                new SnowTileSpec(),
                                coords));
                        break;

                    case 't':
                        objects.add(factory.create(
                                new StalactiteTileSpec(),
                                coords));
                        break;

                    case 'i':
                        objects.add(factory.create(
                                new StalagmiteTileSpec(),
                                coords));
                        break;

                    case 'd':
                        objects.add(factory.create(
                                new DeadTreeTileSpec(),
                                coords));
                        break;

                    case 'e':
                        objects.add(factory.create(
                                new SnowyTreeTileSpec(),
                                coords));
                        break;

                    case 'x':
                        objects.add(factory.create(
                                new CollectibleObjectSpec(),
                                coords));
                        gs.coinAddedToLevel();
                        break;

                    case 'z':
                        objects.add(factory.create(
                                new FireTileSpec(),
                                coords));

                        break;

                    case 'y':
                        objects.add(factory.create(
                                new InvisibleDeathTenByTenSpec(),
                                coords));

                        break;
                    case 'v':
                        objects.add(factory.create(new EnemySpec(), coords));
                        break;
                    case '-':
                        objects.add(factory.create(new LaserSpec(), coords));

                    case '.':
                        // Nothing to see here
                        break;

                    default:
                        Log.e("Unhandled item in level",
                                "row:" + row
                                        + " column:" + column);
                        break;
                }
            }
        }
    }

    /**
     * Spawn laser object
     * @param playerTransform
     */
    public void createLaser(PlayerTransform playerTransform) {
        FIRST_PLAYER_LASER = getGameObjects().size();
        LAST_PLAYER_LASER = FIRST_PLAYER_LASER + 2;
            PointF startPosition = playerTransform.getFiringLocation();
            for (int i = FIRST_PLAYER_LASER; i != LAST_PLAYER_LASER + 1; i++) {
                objects.add(i, factory.create(new LaserSpec(), startPosition));
        }
        createdLaser = true;
        mNextPlayerLaser = FIRST_PLAYER_LASER;
    }
}
