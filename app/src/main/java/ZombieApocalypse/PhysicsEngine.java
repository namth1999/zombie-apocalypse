/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse;

import android.graphics.PointF;
import android.graphics.RectF;

import java.util.ArrayList;

import ZombieApocalypse.Levels.LevelManager;
import ZombieApocalypse.Transform.PlayerTransform;
import ZombieApocalypse.Transform.Transform;

public class PhysicsEngine {
    public static boolean collisionOccurred = false;

    public void update(long fps,
                       ArrayList<GameObject> objects,
                       GameState gs) {

        for (int i=0;i<objects.size();i++) {
                objects.get(i).update(fps,
                        objects.get(LevelManager.PLAYER_INDEX)
                                .getTransform());
        }

        detectCollisions(gs, objects);
    }

    private void detectCollisions(GameState gs,
                                  ArrayList<GameObject> objects) {


        // Something collides with some part of
        // the player most frames
        // so, let's make some handy references
        // Get a reference to the players position
        // as we will probably need to update it
        Transform playersTransform =
                objects.get(LevelManager.PLAYER_INDEX)
                        .getTransform();

        PlayerTransform playersPlayerTransform =
                (PlayerTransform) playersTransform;

        // Get the players extra colliders
        // from the cast object
        ArrayList<RectF> playerColliders =
                playersPlayerTransform.getColliders();

        PointF playersLocation =
                playersTransform.getLocation();

        for (int i=0;i<objects.size();i++) {
            // Just need to check player collisions
            // with everything else
            if (objects.get(i).checkActive()) {
                // Object is active so check for collision
                // with player - anywhere at all
                if (RectF.intersects(objects.get(i).getTransform()
                                .getCollider(),
                        playersTransform
                                .getCollider())) {

                    // A collision of some kind has occurred
                    // so let's dig a little deeper
                    collisionOccurred = true;
                    // Get a reference to the current
                    // (being tested) object's
                    // transform and location
                    Transform testedTransform =
                            objects.get(i).getTransform();
                    PointF testedLocation = testedTransform
                            .getLocation();

                    // Don't check the player against itself
                    if (objects.indexOf(objects.get(i)) !=
                            LevelManager.PLAYER_INDEX) {

                        // Where was the player hit?
                        for (int j = 0; j
                                < playerColliders.size(); j++) {

                            if (RectF.intersects(testedTransform
                                            .getCollider(),
                                    playerColliders.get(j))) {

                                // React to the collision based on
                                // body part and object type

                                switch (objects.get(i).getTag() + " with " + "" + j) {
                                    // Test feet first to avoid the
                                    // player sinking in to a tile
                                    // and unnecessarily triggering
                                    // right and left as well
                                    case "Movable Platform with 0":// Feet
                                        playersPlayerTransform.grounded();
                                        playersLocation.y =
                                                (testedTransform.getLocation().y)
                                                        - (playersTransform.getSize().y);
                                        break;

                                    case "Death with 0":// Feet
                                        gs.death();
                                        break;

                                    case "Inert Tile with 0":// Feet
                                        playersPlayerTransform.grounded();
                                        playersLocation.y =
                                                (testedTransform.getLocation().y)
                                                        - (playersTransform.getSize().y);
                                        break;

                                    case "Inert Tile with 1":// Head
                                        // Just update the player to a suitable height
                                        // but allow any jumps to continue
                                        playersLocation.y = testedLocation.y
                                                + testedTransform.getSize().y;

                                        playersPlayerTransform.triggerBumpedHead();
                                        break;

                                    case "Collectible with 2":// Right
                                        SoundEngine.playCoinPickup();
                                        // Switch off coin
                                        objects.get(i).setInactive();
                                        // Tell the game state a coin has been found
                                        gs.coinCollected();
                                        break;

                                    case "Inert Tile with 2":// Right
                                        // Stop the player moving right
                                        playersTransform.stopMovingRight();
                                        // Move the player to the left of the tile
                                        playersLocation.x = (testedTransform
                                                .getLocation().x)
                                                - playersTransform.getSize().x;
                                        break;

                                    case "Collectible with 3":// Left
                                        SoundEngine.playCoinPickup();
                                        // Switch off coin
                                        objects.get(i).setInactive();
                                        // Tell the game state a coin has been found
                                        gs.coinCollected();
                                        break;

                                    case "Inert Tile with 3":// Left
                                        playersTransform.stopMovingLeft();
                                        // Move the player to the right of the tile
                                        playersLocation.x =
                                                testedLocation.x
                                                        + testedTransform.getSize().x;
                                        break;

                                    // Handle the player's feet reaching
                                    // the objective tile
                                    case "Objective Tile with 0":
                                        SoundEngine.playReachObjective();
                                        gs.objectiveReached();
                                        break;

                                    case "Enemy with 0":
                                        gs.death();
                                        break;

                                    case "Enemy with 2":
                                        gs.death();
                                        break;

                                    case "Enemy with 3":
                                        gs.death();
                                        break;

                                    default:

                                        break;
                                }
                            }
                        }
                    }
                }
                if (objects.get(i).getTag().equals("Enemy")) {
                    //System.out.println("激光测试成功");
                    for (int k = 0; k < objects.size(); k++) {
                        if (objects.get(k).getTag().equals("laser") && objects.get(k).getTransform().getCollider().intersect(objects.get(i).getTransform().getCollider())) {
                            objects.get(i).setInactive();
                            objects.get(k).setInactive();
                            System.out.println("击中成功");
                        }
                    }
                }

            }

        }

        if (!collisionOccurred) {
            // No connection with main player collider so not grounded
            playersPlayerTransform.notGrounded();

        }
    }
}
