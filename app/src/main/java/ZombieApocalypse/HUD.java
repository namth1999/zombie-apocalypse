/*
 * *
 *  * Created by RuiKang Xu
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;

import nl.saxion.playground.template.R;

// Teacher: Usually it is not adviseable to use all capitals in the class name.
public class HUD {
    private Bitmap menuBackgroundBitmap;
    private Bitmap leftBitmap;
    private Bitmap rightBitmap;
    private Bitmap jumpBitmap;
    private Bitmap shootBitmap;

    private Context c;
    private int mTextFormatting;
    private int mScreenHeight;
    private int mScreenWidth;
    private int buttonWidth;
    private int buttonHeight;
    private int buttonPadding;
    final float ONE_THIRD = .33f;
    final float TWO_THIRDS = .66f;


    private ArrayList<Rect> mControls;

    public static int LEFT = 0;
    public static int RIGHT = 1;
    public static int JUMP = 2;
    public static int SHOOT = 3;
//Teacher: How many times do you create object of this class.
    public HUD(Context context, Point size) {
        this.c = context;
        mScreenHeight = size.y;
        mScreenWidth = size.x;
        mTextFormatting = size.x / 25;
        buttonWidth = mScreenWidth / 13;
        buttonHeight = mScreenHeight / 11;
        buttonPadding = mScreenWidth / 89;

        // Create and scale the bitmaps
        menuBackgroundBitmap = BitmapFactory
                .decodeResource(c.getResources(),
                        R.drawable.menubackground);

        menuBackgroundBitmap = Bitmap
                .createScaledBitmap(menuBackgroundBitmap,
                        mScreenWidth, mScreenHeight, true);

        prepareControls();

    }

    private void prepareControls() {

        Rect left = new Rect(
                buttonPadding,
                mScreenHeight - buttonHeight - buttonPadding,
                buttonWidth + buttonPadding,
                mScreenHeight - buttonPadding);

        Rect right = new Rect(
                (buttonPadding * 2) + buttonWidth,
                mScreenHeight - buttonHeight - buttonPadding,
                (buttonPadding * 2) + (buttonWidth * 2),
                mScreenHeight - buttonPadding);

        Rect jump = new Rect(mScreenWidth - buttonPadding - buttonWidth,
                mScreenHeight - buttonHeight - buttonPadding,
                mScreenWidth - buttonPadding,
                mScreenHeight - buttonPadding);

        Rect shoot = new Rect(
                mScreenWidth - 2 * (buttonPadding + buttonWidth),
                mScreenHeight- buttonHeight - buttonPadding,
                mScreenWidth - 2 * buttonPadding - buttonWidth,
                mScreenHeight - buttonPadding
                );


        mControls = new ArrayList<>();
        mControls.add(LEFT, left);
        mControls.add(RIGHT, right);
        mControls.add(JUMP, jump);
        mControls.add(SHOOT,shoot);


        leftBitmap = BitmapFactory
                .decodeResource(c.getResources(),
                        R.drawable.left);

        leftBitmap = Bitmap
                .createScaledBitmap(leftBitmap,
                        buttonWidth, buttonHeight, false);

        rightBitmap = BitmapFactory
                .decodeResource(c.getResources(),
                        R.drawable.right);

        rightBitmap = Bitmap
                .createScaledBitmap(rightBitmap,
                        buttonWidth, buttonHeight, false);

        jumpBitmap = BitmapFactory
                .decodeResource(c.getResources(),
                        R.drawable.jump);

        jumpBitmap = Bitmap
                .createScaledBitmap(jumpBitmap,
                        buttonWidth, buttonHeight, false);

        shootBitmap = BitmapFactory
                .decodeResource(c.getResources(),
                        R.drawable.gun);

        shootBitmap = Bitmap
                .createScaledBitmap(shootBitmap,
                        buttonWidth,buttonHeight,false);
    }

    void draw(Canvas c, Paint p, GameState gs) {

        if (gs.getGameOver()) {

            // Draw the mMenuBitmap screen
            c.drawBitmap(menuBackgroundBitmap, 0, 0, p);

            // draw a rectangle to highlight the text
            p.setColor(Color.argb(100, 26, 128, 182));
            c.drawRect(0, 0, mScreenWidth,
                    mTextFormatting * 4, p);

            // Draw the level names
            p.setColor(Color.argb(255, 255, 255, 255));
            p.setTextSize(mTextFormatting);
            c.drawText("Underground",
                    mTextFormatting,
                    mTextFormatting * 2,
                    p);

            c.drawText("Mountains",
                    mScreenWidth * ONE_THIRD
                            + (mTextFormatting),
                    mTextFormatting * 2,
                    p);

            c.drawText("Graveyard",
                    mScreenWidth * TWO_THIRDS
                            + (mTextFormatting),
                    mTextFormatting * 2,
                    p);

            // Draw the fastest times
            p.setTextSize(mTextFormatting / 1.8f);

            c.drawText("BEST:" + gs.getFastestUnderground()
                            + " seconds",
                    mTextFormatting,
                    mTextFormatting * 3,
                    p);

            c.drawText("BEST:" + gs.getFastestMountains()
                            + " seconds", mScreenWidth * ONE_THIRD
                            + mTextFormatting,
                    mTextFormatting * 3,
                    p);

            c.drawText("BEST:" + gs.getFastestCity()
                            + " seconds",
                    mScreenWidth * TWO_THIRDS + mTextFormatting,
                    mTextFormatting * 3,
                    p);

            // draw a rectangle to highlight the large text
            p.setColor(Color.argb(100, 26, 128, 182));
            c.drawRect(0, mScreenHeight - mTextFormatting * 2,
                    mScreenWidth,
                    mScreenHeight,
                    p);
            p.setColor(Color.argb(255, 255, 255, 255));
            p.setTextSize(mTextFormatting * 1.5f);
            c.drawText("DOUBLE TAP A LEVEL TO PLAY",
                    ONE_THIRD + mTextFormatting * 2,
                    mScreenHeight - mTextFormatting / 2,
                    p);
        } else {
            // draw a rectangle to highlight the text
            p.setColor(Color.argb(100, 0, 0, 0));
            c.drawRect(0, 0, mScreenWidth,
                    mTextFormatting,
                    p);

            // Draw the HUD text
            p.setTextSize(mTextFormatting / 1.5f);
            p.setColor(Color.argb(255, 255, 255, 255));
            c.drawText("Time:" + (gs.getCurrentTime()
                            + gs.getCoinsRemaining() * 10),
                    mTextFormatting / 4,
                    mTextFormatting / 1.5f,
                    p);


            drawControls(c, p);

            c.drawBitmap(leftBitmap,
                    buttonPadding,
                    mScreenHeight - buttonHeight - buttonPadding,
                    p);
            c.drawBitmap(rightBitmap,
                    (buttonPadding * 2) + buttonWidth,
                    mScreenHeight - buttonHeight - buttonPadding,
                    p);
            c.drawBitmap(jumpBitmap,
                    mScreenWidth - buttonPadding - buttonWidth,
                    mScreenHeight - buttonHeight - buttonPadding,
                    p);
            c.drawBitmap(shootBitmap,
                    mScreenWidth - 2 * (buttonPadding + buttonWidth),
                    mScreenHeight- buttonHeight - buttonPadding,
                    p);
        }

    }
//Teacher: The master breach must contain clean code.
    private void drawControls(Canvas c, Paint p) {
//        p.setColor(Color.argb(0, 255, 255, 255));
//
////        for (int i=0;i<mControls.size();i++) {
////            c.drawRect(mControls.get(i).left, mControls.get(i).top, mControls.get(i).right, mControls.get(i).bottom, p);
////        }
//
//        // Set the colors back
//        p.setColor(Color.argb(255, 255, 255, 255));
    }


    ArrayList<Rect> getControls() {
        return mControls;
    }

}