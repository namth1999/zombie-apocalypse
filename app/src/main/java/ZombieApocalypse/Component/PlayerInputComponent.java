/*
 * Created by Ruikang Xu
 * Copyright (c) 2019 . All rights reserved.
 *
 */

package ZombieApocalypse.Component;

import android.graphics.Rect;
import android.view.MotionEvent;

import java.util.ArrayList;

import ZombieApocalypse.GameEngine;
import ZombieApocalypse.GameState;
import ZombieApocalypse.HUD;
import ZombieApocalypse.Interfaces.InputObserver;
import ZombieApocalypse.Interfaces.PlayerLaserSpawner;
import ZombieApocalypse.Levels.LevelManager;
import ZombieApocalypse.SoundEngine;
import ZombieApocalypse.Transform.PlayerTransform;
import ZombieApocalypse.Transform.Transform;

public class PlayerInputComponent implements InputObserver {
    private Transform mPlayersTransform;
    private PlayerTransform mPlayersPlayerTransform;
    private PlayerLaserSpawner mPLS;
    public static boolean right = true;
    public static boolean fired;

    public PlayerInputComponent(GameEngine ger) {
        ger.addObserver(this);
        mPLS = ger;
    }

    public void setTransform(Transform transform) {
        mPlayersTransform = transform;
        mPlayersPlayerTransform =
                (PlayerTransform) mPlayersTransform;
    }

    @Override
    public void handleInput(MotionEvent event, GameState gs, ArrayList<Rect> buttons) {
        int i = event.getActionIndex();
        int x = (int) event.getX(i);
        int y = (int) event.getY(i);


        if (!gs.getPaused()) {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    if (buttons.get(HUD.LEFT).contains(x, y)
                            || buttons.get(HUD.RIGHT)
                            .contains(x, y)) {
                        // Player has released
                        // either left or right

                        mPlayersTransform.stopHorizontal();
                    }
                    break;

                case MotionEvent.ACTION_DOWN:
                    if (buttons.get(HUD.LEFT).contains(x, y)) {
                        // Player has pressed left
                        mPlayersTransform.headLeft();
                        right = false;
                    } else if (buttons.get(HUD.RIGHT).contains(x, y)) {
                        // Player has pressed right
                        mPlayersTransform.headRight();
                        right = true;
                    } else if (buttons.get(HUD.JUMP).contains(x, y)) {
                        // Player has released
                        // the jump button
                        mPlayersPlayerTransform.triggerJump();
                    } else if (buttons.get(HUD.SHOOT).contains(x, y)) {
                        if (!LevelManager.createdLaser){
                            GameEngine.getLM().createLaser(mPlayersPlayerTransform);
                        }
                        if (!fired){
                            mPLS.spawnPlayerLaser(mPlayersPlayerTransform);
                            SoundEngine.playShoot();
                            fired = true;
                        }
                    }
                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    if (buttons.get(HUD.LEFT).contains(x, y)
                            || buttons.get(HUD.RIGHT)
                            .contains(x, y)) {
                        // Player has released either
                        // up or down
                        mPlayersTransform.stopHorizontal();
                    }
                    break;

                case MotionEvent.ACTION_POINTER_DOWN:
                    if (buttons.get(HUD.LEFT).contains(x, y)) {
                        // Player has pressed left
                        mPlayersTransform.headLeft();
                        GameEngine.getLM().getGameObjects().get(LevelManager.mNextPlayerLaser).getTransform().headLeft();
                    } else if (buttons.get(HUD.RIGHT).contains(x, y)) {
                        // Player has pressed right
                        mPlayersTransform.headRight();
                        GameEngine.getLM().getGameObjects().get(LevelManager.mNextPlayerLaser).getTransform().headRight();
                    } else if (buttons.get(HUD.JUMP).contains(x, y)) {
                        // Player has released
                        // the jump button
                        mPlayersPlayerTransform.triggerJump();
                    } else if (buttons.get(HUD.SHOOT).contains(x, y)) {
                        if (!LevelManager.createdLaser){
                            GameEngine.getLM().createLaser(mPlayersPlayerTransform);
                        }
                        if (!fired){
                            mPLS.spawnPlayerLaser(mPlayersPlayerTransform);
                            SoundEngine.playShoot();
                            fired = true;
                        }
                    }
                    break;
            }
        }
    }
}
