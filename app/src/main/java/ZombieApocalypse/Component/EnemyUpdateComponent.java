/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Component;

import android.graphics.PointF;

import ZombieApocalypse.Interfaces.UpdateComponent;
import ZombieApocalypse.Transform.Transform;

public class EnemyUpdateComponent implements UpdateComponent {
    @Override
    public void update(long fps, Transform t, Transform playerTransform) {
        PointF location = t.getLocation();
        if (t.headingRight()) {
            location.x += t.getSpeed() / fps;
        } else if (t.headingLeft()) {
            location.x -= t.getSpeed() / fps;
        } else {
            // Must be first update of the game
            // so start with going left
            t.headLeft();
        }

        // Check if the platform needs
        // to change direction
        if (t.headingLeft() && location.x <=
                t.getStartingPosition().x - t.getSize().x * 6) {

            // Back at the start
            t.headRight();
        } else if (t.headingRight() && location.x >=
                (t.getStartingPosition().x
                        + t.getSize().x * 6)) {

            // Moved ten times the height downwards
            t.headLeft();
        }

        // Update the colliders with the new position
        t.updateCollider();
    }
}
