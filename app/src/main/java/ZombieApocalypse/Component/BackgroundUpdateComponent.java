/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Modified by Xu Ruikang
 */
package ZombieApocalypse.Component;

import ZombieApocalypse.Interfaces.UpdateComponent;
import ZombieApocalypse.PhysicsEngine;
import ZombieApocalypse.Transform.BackgroundTransform;
import ZombieApocalypse.Transform.PlayerTransform;
import ZombieApocalypse.Transform.Transform;

public class BackgroundUpdateComponent implements UpdateComponent {
    @Override
    public void update(long fps, Transform t, Transform playerTransform) {
        // Cast to background transform
        BackgroundTransform bt =
                (BackgroundTransform) t;

        PlayerTransform pt =
                (PlayerTransform) playerTransform;

        float currentXClip = bt.getXClip();

        // When the player is moving right -
        // update currentXClip to the left
        //只有当玩家没有被阻挡的时候背景才会被更新
        if (playerTransform.headingRight() && !PhysicsEngine.collisionOccurred) {
            currentXClip -= t.getSpeed() / fps;
            bt.setXClip(currentXClip);
        }

        // When the player is heading left -
        // update currentXClip to the right
        //只有当玩家没有被阻挡的时候背景才会被更新
        else if (playerTransform.headingLeft() && !PhysicsEngine.collisionOccurred) {
            currentXClip += t.getSpeed() / fps;
            bt.setXClip(currentXClip);
        }

        // When currentXClip reduces either
        // extreme left or right
        // Flip the order and reset currentXClip
        if (currentXClip >= t.getSize().x) {
            bt.setXClip(0);
            bt.flipReversedFirst();
        } else if (currentXClip <= 0) {
            bt.setXClip((int) t.getSize().x);
            bt.flipReversedFirst();
        }
    }
}
