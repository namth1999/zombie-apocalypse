/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Component;

import android.graphics.PointF;

import ZombieApocalypse.Interfaces.SpawnComponent;
import ZombieApocalypse.Transform.PlayerTransform;
import ZombieApocalypse.Transform.Transform;

public class LaserSpawnComponent implements SpawnComponent {

    @Override
    public void spawn(PlayerTransform playerTransform,
                      Transform t) {
        PointF startPosition =
                playerTransform.getFiringLocation();

        t.setLocation((int)startPosition.x, (int)startPosition.y);

        if(playerTransform.getFacingRight()){
            t.headRight();
        }
        else {
            t.headLeft();
        }
    }
}