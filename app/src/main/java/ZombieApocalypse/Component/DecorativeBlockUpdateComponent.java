/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Component;

import ZombieApocalypse.Interfaces.UpdateComponent;
import ZombieApocalypse.Transform.Transform;

public class DecorativeBlockUpdateComponent implements UpdateComponent {
    @Override
    public void update(long fps, Transform t, Transform playerTransform) {

    }
}
