/*
 * *
 *  * Created by Nam Trinh
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Component;

import android.graphics.PointF;

import ZombieApocalypse.GameEngine;
import ZombieApocalypse.Interfaces.UpdateComponent;
import ZombieApocalypse.Levels.LevelManager;
import ZombieApocalypse.Transform.Transform;

public class LaserUpdateComponent implements UpdateComponent {
    private final long MAX_SHOOT_TIME = 1000;
    private long shootStartTime;

    @Override
    public void update(long fps, Transform t, Transform playerTransform) {
        float range = t.getLocation().x + 1000;
//        GameEngine.getLM().getGameObjects().get(LevelManager.mNextPlayerLaser).setActive();

        // Where is the laser
        PointF location = t.getLocation();

        // How fast is it going
        float speed = t.getSpeed();

//        if (playerTransform.headingLeft()) {
//            t.headLeft();
//        } else if (playerTransform.headingRight()) {
//            t.headRight();
//        }

        if (PlayerInputComponent.right) {
            location.x += speed / fps;
            t.updateCollider();
            shootStartTime = System.currentTimeMillis();
            if (System.currentTimeMillis() > shootStartTime + MAX_SHOOT_TIME) {
                    GameEngine.getLM().getGameObjects().get(LevelManager.FIRST_PLAYER_LASER).setInactive();
                PlayerInputComponent.fired = false;
            }

        } else if (!PlayerInputComponent.right) {
            location.x -= speed / fps;
            t.updateCollider();
        }

//         Has the laser gone out of range
//        if (location.x < -range || location.x > range) {
//            GameEngine.getLM().getGameObjects().get(LevelManager.LAST_PLAYER_LASER).setInactive();
//                PlayerInputComponent.fired = false;
//        }

    }
}
