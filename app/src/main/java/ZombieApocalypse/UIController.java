/*
 * *
 *  * Created by Linh Hoang
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */

package ZombieApocalypse;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;

import java.util.ArrayList;

import ZombieApocalypse.Interfaces.GameEngineBroadcaster;
import ZombieApocalypse.Interfaces.InputObserver;

class UIController implements InputObserver {

    private float mThird;

    private boolean initialPress = false;

    /**
     * observe input
     * @param b
     * @param size
     */
    UIController(GameEngineBroadcaster b, Point size) {
        // Add as an observer
        addObserver(b);

        mThird = size.x / 3;
    }

    /**
     * add observer method
     * @param b
     */
    void addObserver(GameEngineBroadcaster b) {
        b.addObserver(this);
    }

    /**
     * Handle input using motion event
     * @param event
     * @param gameState
     * @param buttons
     */
    @Override
    public void handleInput(MotionEvent event,
                            GameState gameState,
                            ArrayList<Rect> buttons) {

        int i = event.getActionIndex();
        int x = (int) event.getX(i);

        int eventType = event.getAction()
                & MotionEvent.ACTION_MASK;

        if (eventType == MotionEvent.ACTION_UP ||
                eventType == MotionEvent.ACTION_POINTER_UP) {

            // If game is over start a new game
            if (gameState.getGameOver() && initialPress) {

                if (x < mThird) {
                    gameState.setCurrentLevel("underground");
                    gameState.startNewGame();
                } else if (x >= mThird && x < mThird * 2) {
                    gameState.setCurrentLevel("mountains");
                    gameState.startNewGame();
                } else if (x >= mThird * 2) {
                    gameState.setCurrentLevel("grave");
                    gameState.startNewGame();
                }
            }
            initialPress = !initialPress;
        }
    }
}
