/*
 * *
 *  * Created by Huy Cao
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Interfaces;

public interface EngineController {
    // This allows the GameState class to start a new level via GameEngine
    void startNewLevel();
}
