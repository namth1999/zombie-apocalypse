/*
 * *
 *  * Created by Huy Cao
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Interfaces;

import ZombieApocalypse.Transform.PlayerTransform;
import ZombieApocalypse.Transform.Transform;

public interface SpawnComponent {
    void spawn(PlayerTransform playerTransform,
               Transform t);
}
