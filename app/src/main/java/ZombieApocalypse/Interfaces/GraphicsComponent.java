/*
 * *
 *  * Created by Huy Cao
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Interfaces;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import ZombieApocalypse.Camera;
import ZombieApocalypse.GOSpec.GameObjectSpec;
import ZombieApocalypse.Transform.Transform;

public interface GraphicsComponent {
    // Added int mPixelsPerMetre to scale the bitmap to the camera
    void initialize(Context c, GameObjectSpec spec,
                    PointF objectSize, int pixelsPerMetre);

    // Updated to take a reference to a Camera
    void draw(Canvas canvas, Paint paint,
              Transform t, Camera cam);
}
