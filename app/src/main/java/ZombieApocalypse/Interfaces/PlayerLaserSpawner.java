/*
 * *
 *  * Created by Huy Cao
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Interfaces;

import ZombieApocalypse.Transform.PlayerTransform;

public interface PlayerLaserSpawner {
    boolean spawnPlayerLaser(PlayerTransform playerTransform);
}
