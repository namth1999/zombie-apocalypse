/*
 * *
 *  * Created by Huy Cao
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */
package ZombieApocalypse.Interfaces;

import ZombieApocalypse.Transform.Transform;

public interface UpdateComponent {
    void update(long fps, Transform t, Transform playerTransform);
}
