/*
 * *
 *  * Created by Linh Hoang
 *  * Copyright (c) 2019 . All rights reserved.
 *
 */

package ZombieApocalypse.Interfaces;

import android.graphics.Rect;
import android.view.MotionEvent;

import java.util.ArrayList;

import ZombieApocalypse.GameState;

public interface InputObserver {
    // This allows InputObservers to be called by GameEngine
    //to handle input
    void handleInput(MotionEvent event,
                     GameState gs, ArrayList<Rect> buttons);
}
